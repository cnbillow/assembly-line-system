<?php

/*
 * 增加，删除，产品交易记录
 * $statement:操作判断条件
 */

include_once $_SERVER['DOCUMENT_ROOT'] . '/class/Product_record.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/class/customer_order.php';
$statement = $_POST['statement'];
$products = $_POST['products']; //二维数组，传入成品的id，价格，数量

if ($statement == '已发货') { //增加
    if (Product_record::add_a_record($products)) {//生成交易记录
        $orderInfo = Db::get_instance()->query("SELECT * FROM fac_consumer_order WHERE consumer_order_id=" . $_POST['order_id']);
        if ($orderInfo[0]['is_proxy'] == 0) {
            Consumer_order::char_state($products); //减去成品库存
        }
        echo 1;
    } else {
        echo 0;
    }
} elseif ($statement == 'delete') {//删除
    if (Product_record::del_a_record($trade_id)) {
        echo 1;
    } else {
        echo 0;
    }
}
