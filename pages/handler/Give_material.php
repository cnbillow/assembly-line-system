<?php

/*
 * 材料需求采购单
 * 新建，删除，修改，材料需求采购单
 * $id材料条目
 * $material_id材料id
 * $number//采购数量

 */

header('Content-Type: text/html; charset=utf-8');
include_once $_SERVER['DOCUMENT_ROOT'] . '/class/Customer.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/class/Material_demand.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/class/department.php';

$id = $_POST['id'];
$remark = $_POST['remark']; //备注
$materials = $_POST['materials']; //材料二维数组
$statement = $_POST['statement'];
$department_id = $_POST['department_id'];
//$department_id = department::getUserDepartment(intval($_COOKIE['userid']));

switch ($statement) {
    case 'add':
        if (Material_demand::material_update($materials) && Material_demand::give_material_record($department_id, $materials)) {
            echo 1;
        } else {
            echo 0;
        }
        break;

    default :
        echo 0;
        break;
}