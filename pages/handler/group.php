<?php

/* * **********已测试成功
 * To change this license header, choose License Headers in Project Properties.
 * 增加，删除，修改，客户分组
 * $statement:操作判断条件
 */
header('Content-Type: text/html; charset=utf-8');
include_once $_SERVER['DOCUMENT_ROOT'] . '/class/Group.php';
$statement = $_POST['statement'];
$group_id = $_POST['id'];
$name = $_POST['name'];

switch ($statement) {
    case 'add':
        if (Group::add_a_group($name)) {
            echo 1;
        } else {
            echo 0;
        }
        break;

    case 'delete':
        if (Group::delete($group_id)) {
            echo 1;
        } else {
            echo 0;
        }
        break;
    case 'update':
        $rs = Group::get_a_group($group_id);
        $rs->group_name = $name;
        if ($rs->update()) {
            echo 1;
        } else {
            echo 0;
        }
        break;
    default :
        echo 0;
        break;
}