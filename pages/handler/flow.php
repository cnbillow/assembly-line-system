<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * 增加，删除，修改，产品流水线
 * $statement:操作判断条件
 */

header('Content-Type: text/html; charset=utf-8');
include_once $_SERVER['DOCUMENT_ROOT'] . '/class/flow.php';
$statement = $_POST['statement'];
$id = $_POST['id'];
$Name = $_POST['name'];
$departments = $_POST['departments'] ? $_POST['departments'] : ''; //$departments;记录流水线的详细信息的二维数组（department_id,sort）
$rs = new flow();
# var_dump($_POST);
switch ($statement) {
    case 'add':
        if ($rs->insert($Name, $departments)) {
            echo 1;
        } else {
            echo 0;
        }
        break;
    case 'delete':
        if ($rs->delete($id)) {
            echo 1;
        } else {
            echo 0;
        }
        break;
    case 'update':
        if ($rs->update($id, $Name)) {
            echo 1;
        } else {
            echo 0;
        }
        break;
    case 'save_item':

        if ($rs->save_department($id, $departments)) {
            echo 1;
        } else {
            echo 0;
        }
        break;
    default :
        echo 0;
}