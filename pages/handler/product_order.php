<?php

/*
 * 生产订单
 * 新建，删除，修改，生产订单
 *  $id;订单号
  $flow_id;流水线号
  $product_id;产品号
  $number;产品数量
  $create_date;下单时间
  $order_state;订单状态
  $finish_date;完成时间
 */

header('Content-Type: text/html; charset=utf-8');
include_once $_SERVER['DOCUMENT_ROOT'] . '/class/Product_order.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/class/Product_order_item.php';

$id = $_POST['id'];
$statement = $_POST['statement'];
$flow_id = 0;
$number = $_POST['number'];
$product_id = $_POST['product_id'];
$order_state = $_POST['order_state'];
$remark = $_POST['remark'];
$create_date = $_POST['create_date'];
$finish_date = $_POST['finish_date'];

$finish_number = $_POST['finish_number'];

$code = $_POST['code'];
// array,
$materials = $_POST['materials']; //接收的是一个数组

switch ($statement) {
    case 'add':   // 创建生产订单 
        if (Product_order::add_a_order($product_id, $code, $number, $flow_id, $remark)) {
            echo 1;
        } else {
            echo 0;
        }
        break;
    case 'delete'://删除
        echo Product_order::delete($id);
        break;
    case 'update'://更新订单
        $rs = Product_order::get_product_order($id);
        $rs->flow_id = $flow_id;
        $rs->number = $number;
        $rs->product_id = $product_id;
        $rs->order_state = $order_state;
        Product_order::save_material_item($id, $materials);
        if ($rs->update()) {
            echo 1;
        } else {
            echo 0;
        }
        break;
    case 'finish':
        $rs = Product_order::get_product_order($id);
        echo $rs->finish($finish_number) ? 1 : 0;
        break;
    default :
        echo 0;
        break;
}