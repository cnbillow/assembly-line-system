<?php

/**
 * 车间生产录入
 */
header('Content-Type: text/html; charset=utf-8');
include_once $_SERVER['DOCUMENT_ROOT'] . '/class/department.php';

$id = $_POST['id'];                 //记录id
$statement = $_POST['statement'];
$department_id = $_POST['department_id'];
$product_id = $_POST['product_id'];
$number = $_POST['number'];
$order_id = isset($_POST['order_id']) && is_numeric($_POST['order_id']) ? intval($_POST['order_id']) : 0;
//$department_id = department::getUserDepartment(intval($_COOKIE['userid']));

switch ($statement) {
    case 'add':
        echo department::saveDepPro($department_id, $product_id, $number, $order_id);
        break;
    case 'delete':
        if (department::delDepPro($id) > 0) {
            echo 1;
        } else {
            echo 0;
        }
        break;
    default :
        echo 0;
        break;
}