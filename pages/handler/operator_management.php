<?php

/* * **********已测试成功
 * To change this license header, choose License Headers in Project Properties.
 * 增加，删除，修改用户
 * $statement:操作判断条件
 */
header('Content-Type: text/html; charset=utf-8');
include_once $_SERVER['DOCUMENT_ROOT'] . '/class/Users.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/class/User_type.php';
$statement = $_POST['statement'];
$c_name = $_POST['name'];
$c_password =  $_POST['password'];
$user_type_id = $_POST['user_type_id'];
$id = $_POST['id'];
$department_id = $_POST['department_id'];

switch ($statement) {

    case 'add';
        $user_id = User::add_a_user($c_name, $c_password, $user_type_id);
        if ($user_id > 0) {
            if (isset($_POST['department_id'])) {
                if (User_type::add_user_department($user_id, $department_id)) {
                    echo 1;
                } else {
                    echo 0;
                }
            } else {
                echo 1;
            }
        } else {
            echo 0;
        }
        break;


    case 'delete':
        if (User::unreg($id)) {
            echo 1;
        } else {

            echo 0;
        }
        break;

    case 'update':
        $rs = User::get_user($id);
        $rs->user_name = $c_name;
        $rs->user_password = $c_password;
        $rs->user_type_id = $user_type_id;
        if($user_type_id == 4){
            $rs->user_department_id = $department_id;
        }
        if ($rs->update()) {
            echo 1;
        } else {
            echo 0;
        }
        break;
    default :
        echo 0;
        break;
}