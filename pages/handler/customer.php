<?php

/* 已测试成功
 * To change rs license header, choose License Headers in Project Properties.
 * 增加，删除，修改，客户资料
 * $statement:操作判断条件
 */
header('Content-Type: text/html; charset=utf-8');
include_once $_SERVER['DOCUMENT_ROOT'] . '/class/Customer.php';
$statement = $_POST['statement'];
$customer_id = $_POST['id'];  //客户id
$customer_name = $_POST['name']; //客户名称
$tel = $_POST['tel'];   //客户电话
$e_mail = $_POST['e_mail'];
$address = $_POST['address'];  //客户地址
$group_id = $_POST['gid'];  //分组id
$remark = $_POST['remark'];   //备注
$qq = $_POST['qq'];

switch ($statement) {
    case 'add':
        if (Customer::add_a_customer($customer_name, $tel, $qq, $e_mail, $address, $remark, $group_id)) {
            echo 1;
        } else {
            echo 0;
        }
        break;
    case 'delete':
        if (Customer::delete_a_consumer($customer_id)) {
            echo 1;
        } else {
            echo 0;
        }
        break;
    case 'update':
        $rs = Customer::get_Customer($customer_id);
        $rs->customer_name = $customer_name;
        $rs->tel = $tel;
        $rs->e_mail = $e_mail;
        $rs->address = $address;
        $rs->group_id = $group_id;
        $rs->remark = $remark;
        $rs->qq = $qq;
        $rs->customer_id = $customer_id;

        if ($rs->update()) {
            echo 1;
        } else {
            echo 0;
        }
        break;
    default :
        echo 0;
        break;
}