<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * 
 * $department_id 车间ID
 * $order_id  订单ID
 * $product_yes合格品
 * $product_no不合格品
 * $product_wait 未加工产品
 * $worktime工作时间
 * $taskstate工作状态
 */
header('Content-Type: text/html; charset=utf-8');
include_once $_SERVER['DOCUMENT_ROOT'] . '/class/order_department.php';
$department_id = $_POST['department_id'];
$order_id = $_POST['order_id'];
$product_yes = $_POST['product_yes'];
$product_no = $_POST['product_no'];
$product_wait = $_POST['product_wait'];
$worktime = $_POST['worktime'];
$taskstate = $_POST['taskstate'];
$statement = $_POST['statement'];
$sort = $_POST['sort'];
$cid = intval($_POST['cid']);

if ($statement == 'update') {
    if (order_department::update($cid, $sort, $product_yes, $product_no, $product_wait, $worktime)) {
        echo 1;
    } else {
        echo 0;
    }
} else if ($statement == 'state_update') {
    echo order_department::state_update($cid, $taskstate, $worktime);
} else if ($statement == 'finish') {
    echo order_department::finish($cid, $order_id);
}
