<?php

/*
 * 处理生产仓库入库
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

include dirname(__FILE__) . '/../../class/_core.php';

$db = Db::get_instance();

$count = intval($_POST['count']);
$productId = intval($_POST['productId']);
$remark = strip_tags($_POST['remark']);

// 最后一个仓库
$lastStore = intval($db->getOne("SELECT `department_id` FROM fac_department WHERE is_final = 1 LIMIT 1;"));
// 最后一个仓库对应库存量
$lastStoreCount = intval($db->getOne("SELECT `finish_number` FROM fac_department_storehouse WHERE department_id = $lastStore;"));

if ($lastStore > 0) {
    // 减库存
    $ret1 = $db->query("UPDATE `fac_department_storehouse` SET `finish_number` = `finish_number` - $count WHERE department_id = $lastStore AND product_id = $productId;");
    if ($ret1) {
        $ret2 = $db->query("UPDATE `fac_product_mess` SET `number` = `number` + $count WHERE product_id = $productId;");
        if ($ret2) {
            echo 1;
        } else {
            // 错误回滚
            $db->query("UPDATE `fac_department_storehouse` SET `finish_number` = `finish_number` + $count WHERE department_id = $lastStore AND product_id = $productId;");
            echo 0;
        }
    } else {
        echo 0;
    }
} else {
    echo 0;
}