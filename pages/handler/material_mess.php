<?php

/* * **********已测试成功
 * To change this license header, choose License Headers in Project Properties.
 * 增加，删除，修改材料库存
 * $statement:操作判断条件
 */



header('Content-Type: text/html; charset=utf-8');
include dirname(__FILE__) . '/../../class/Material.php';
$id = $_POST['id']; //材料id
$statement = $_POST['statement']; //执行操作的条件
$name = $_POST['name']; //材料名
$type = $_POST['type'];
$number = $_POST['number']; //库存数量
$code = $_POST['code']; 
$remark = $_POST['remark']; //备注
$num = $_POST['num']; //材料退货数量


switch ($statement) {
    case 'add'://添加原料
        echo Material::add_a_material($name, $type, $number, $code, $remark);
        break;
    case 'update'://原料更新
        $u = Material::get_Material($id);
        $u->material_name = $name;
        $u->type = $type;
        $u->material_code = $code;
        $u->number = $number;
        $u->remark = $remark;
        if ($u->update()) {
            echo '1';
        } else {
            echo "0";
        }
        break;
    case 'delete'://原料删除
        if (Material::delete_a_material($id)) {
            echo 1;
        } else {
            echo 0;
           }
        break;
    case 'take_back'://原料退货
        $rs = Material::get_Material($id);
        if ($rs->take_back($num)) {
            echo 1;
        } else {
            echo 0;
        }
    default:
        echo 0;
        break;
}

