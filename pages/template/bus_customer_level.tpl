<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8" />
        <title>客户管理</title>
        {include file="head_iframe.tpl"}
    </head>
    <body class="iframe_body">
        <div id="iframe_height_set" class="clearfix">
            <div style="float: left;">

                <div class="div_working">
                    {foreach from=$flow item=step}
                        <a class="a_working">{$step.flow_name}</a>
                    {/foreach}
                </div>
            </div>
            <div>
                <div style="margin-top: 10px;">
                    <table class="dTable">
                        <thead>
                            <tr>
                                <td>顺序</td>
                                <td>车间</td>
                                <td>备注</td>
                            </tr>
                        </thead>
                        <tbody>
                            {foreach from=$flow item=gan}
                                <tr>
                                    <td>{$gan.flow_id}</td>
                                    <td>{$gan.flow_name}</td>
                                    <td></td>
                                </tr>
                            {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div id="bottom">
            <div id="product">
                <a>客户等级:</a>
                <a class="button" >添加</a>
                <a class="button" >修改</a>
                <a class="button" >删除</a>
            </div>
            <div id="setup">
                <a>客户资料:</a>
                <a class="button" >添加</a>
                <a class="button" >修改</a>
                <a class="button" >删除</a>
            </div>
        </div>
    </body>
</html>
