<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8" />
        <title>{$title}</title>
        {include file="head_iframe.tpl"}
    </head>
    <body class="iframe_body">
        <table width="100%" class="ltable">
            <caption>系统总览</caption>
            <tr>
                <th>产品型号</th>
                <th>产品编号</th>
                <th>产品规格</th>
                <th>目前库存</th>
                <th>本月生产</th>
                <th>本月销量</th>
                <th>本月销售</th>
            </tr>
            {foreach from=$data1 item=data}
                <tr>
                    <td>{$data.product_model}</td>
                    <td>{$data.product_code}</td>
                    <td>{$data.gongyi}</td>
                    <td>{$data.number} 件</td>
                    <td>{$data.mproduce} 件</td>
                    <td>{$data.msalec} 件</td>
                    <td class="prices">&yen;{$data.msale}</td>
                </tr>
            {/foreach}
        </table>
        <div class="clearfix" style="margin-top:15px;">
            {foreach from=$data2 item=data name=foa}
                <div class="f{if $smarty.foreach.foa.index is odd}r{else}l{/if}50">
                    <div class="m15 m{if $smarty.foreach.foa.index is odd}l{else}r{/if}7">
                        <table width="100%" class="ltable">
                            <caption class="minCaption">{$data.department_name}</caption>
                            <tr>
                                <th>产品型号</th>
                                <th>未加工</th>
                                <th>已加工</th>
                                <th>日产量</th>
                                <th>月产量</th>
                            </tr>
                            {foreach from=$data['list'] item=da}
                                <tr>
                                    <td>{$da.product_model}</td>  
                                    <td>{$da.unfins}</td>
                                    <td>{$da.fins}</td>
                                    <td>{$da.dpd} 件</td>
                                    <td>{$da.mpd} 件</td>
                                </tr>
                            {/foreach}
                        </table>
                    </div>
                </div>
            {/foreach}
        </div>
        <script type="text/javascript">
            $(function() {
                dataTableLis('.ltable', false);
            });
        </script>
    </body>
</html>