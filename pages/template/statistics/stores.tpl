<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8" />
        <title>{$title}</title>
        {include file="head_iframe.tpl"}
        <script type="text/javascript" onerror="" src="/scripts/js/highcharts/js/highcharts.js"></script>
        <script type="text/javascript" src="/scripts/js/statistics/stores.js"></script>
    </head>
    <body class="iframe_body">
        <div class="iframeTop">
            <div style='float:left;line-height: 45px;margin-left: 10px;'>
                仓库共有 {$count} 产品
            </div>
            <a class="button" href="/pages/product_storehouse.php">查看产品库存</a>
        </div>
        <div id="stores_1"></div>
        <div class="clearfix" style="border-top: 1px solid #dedede;">
            <div id="product_orders"></div>
        </div>
    </body>
</html>