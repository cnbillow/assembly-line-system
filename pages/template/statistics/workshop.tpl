<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8" />
        <title>{$title}</title>
        {include file="head_iframe.tpl"}
        <script type="text/javascript" onerror="" src="http://cdn.hcharts.cn/highcharts/4.0.3/highcharts.js"></script>
        <script type="text/javascript" src="/scripts/js/statistics/workshop.js"></script>
    </head>
    <body class="iframe_body">
        <div id="container"></div>
    </body>
</html>