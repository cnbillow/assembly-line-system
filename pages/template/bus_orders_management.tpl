<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8" />
        <title>客户订单</title>
        {include file="head_iframe.tpl"}
    </head>
    <body class="iframe_body">
        <div id="iframe_height_set">
            <div>
                <table class="dTable" width="100%">
                    <thead>
                        <tr class="custom_orders_tr">
                            <td>订单号</td>
                            <td>客户</td>                          
                            <td>数量</td>
                            <td>下单时间</td>
                            <td>完成时间</td>
                            <td>备注</td>                         
                            <td>状态</td>                           
                        </tr>
                    </thead>
                    <tbody>
                        {foreach from=$order item=gan}
                            <tr class="custom_orders_tr">
                                <td>{$gan.consumer_order_id}</td>
                                <td>{$gan.customer_name}</td>
                                <td>{$gan.sum}</td>
                                <td>{$gan.create_date}</td>              
                                <td>{$gan.finish_date}</td>   
                                <td>{$gan.remark}</td>
                                <td>{$gan.order_state}</td>                    
                            </tr>
                        {/foreach}
                    </tbody>
                </table>
            </div>
        </div>
        <div id="bottom">
            <a class="button">查看库存</a>
            <a class="button">添加订单</a>
            <a class="button" >查看详情</a>
        </div>
    </body>
</html>
