<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8" />
        <title>原料交易记录</title>
        {include file="head_iframe.tpl"}
    </head>
    <body class="iframe_body">
        <div id="iframe_height_set">
            <table class="dTable" width="100%">
                <thead>  
                    <tr>
                        <th style="display: none">单号</th>
                        <th>名称</th>
                        <th>规格</th>
                        <th>数量</th>
                        <th>金额</th>
                        <th>操作人</th>
                        <th>时间</th>
                    </tr>
                </thead>
                <tbody> 
                    {foreach from=$material item=trade}
                        <tr>
                            <td style="display: none">{$trade.trade_id}</td>
                            <td>{$trade.material_name}</td>
                            <td>{$trade.material_type}</td>
                            <td>{$trade.trade_number}</td>
                            <td class="prices out">&yen;{$trade.price}</td>
                            <td>{$trade.operator}</td>
                            <td>{$trade.trade_date}</td>                   
                        </tr>
                    {/foreach}
                </tbody>
            </table>
        </div>
    </body>
</html>
