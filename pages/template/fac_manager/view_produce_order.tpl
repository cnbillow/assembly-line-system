<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="refresh" content="10"/>
        <title>{$title}</title>
        {include file="head_iframe.tpl"}
    </head>
    <body class="iframe_body">
        <div class="iframeTop">
            <a class="button" href="javascript:;" onclick="history.go(-1)">返回</a>
        </div>
        <div style="padding:20px;">
            <table width="100%" style="border-top: 1px solid #dedede;border-right: 1px solid #dedede;box-shadow: 0 1px 3px rgba(0,0,0,0.1);;">
                <thead>
                    <tr class="product_orders_tr" style="border-left: 1px solid #dedede;">
                        <td>订单号</td>
                        <td>成品名称</td>            
                        <td>成品规格</td>
                        <td>预计数量</td>
                        <td>流水线</td>
                        <td>状态</td>
                    </tr>
                </thead>
                <tbody> 
                    <tr class="product_orders_tr" style="border-left: 1px solid #dedede;">
                        <td>{$orderData.product_order_id}</td>
                        <td>{$orderData.product_model}</td>
                        <td>{$orderData.gongyi}</td>
                        <td>{$orderData.pnumber}</td>
                        <td>{$orderData.flow_name}</td>
                        <td>{$orderData.order_state}</td>      
                    </tr>
                </tbody>
            </table>
            <table width="100%" style="margin-top: 15px;border-top: 1px solid #dedede;border-right: 1px solid #dedede;box-shadow: 0 1px 3px rgba(0,0,0,0.1);;">
                <thead>
                    <tr class="product_orders_tr" style="border-left: 1px solid #dedede;">
                        <td>原料名称</td>            
                        <td>原料规格</td>
                        <td>使用数量</td>
                    </tr>
                </thead>
                <tbody> 
                    {section name=mi loop=$materials}
                        <tr class="product_orders_tr" style="border-left: 1px solid #dedede;">
                            <td>{$materials[mi].material_name}</td>
                            <td>{$materials[mi].material_type}</td>
                            <td>{$materials[mi].number}</td>  
                        </tr>
                    {/section}
                </tbody>
            </table>
            <div>
                {section name=dd loop=$status}
                    <div class="dpStatus s{$status[dd].statusint}">
                        <div>{$status[dd].department_name} 
                            <span class="statusCap">&lt;{$status[dd].taskstate} {$status[dd].percent}%&gt;</span>
                        </div>
                        <div class="dpStatusItem">
                            合格品：{$status[dd].product_yes}
                        </div>
                        <div class="dpStatusItem">
                            不合格品：{$status[dd].product_no}
                        </div>
                        <div class="dpStatusItem">
                            未完成：{$status[dd].product_wait}
                        </div>
                        <div class="dpStatusItem">
                            耗时：{$status[dd].worktime}
                        </div>
                    </div>
                    <div class="pcent s{$status[dd].statusint}" style="width: {$status[dd].percent}%"></div>
                {/section}
            </div>
        </div>
    </body>
</html>