<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8" />
        <title>{$title}</title>
        {include file="head_iframe.tpl"}
    </head>
    <body class="iframe_body">
        <div id="_topLeftPlace">仓库共有 {$count}件 原料</div>
        <div id="iframe_height_set">
            <table class="dTable" width="100%">
                <thead>
                    <tr>
                        <th>原料编号</th>
                        <th>原料名称</th>
                        <th>规格</th>
                        <th>备注</th>
                        <th>库存</th>
                    </tr>
                </thead>
                <tbody>
                    {strip}
                        {foreach from=$material item=gan}
                            <tr>
                                <td>{$gan.material_code}</td>
                                <td data-id="{$gan.material_id}">{$gan.material_name}</td>
                                <td>{$gan.material_type}</td>
                                <td style="width: 200px;">
                                    <a class="Elipsis" title="{$gan.remark}" href="javascript:;" style="width: 200px;display: block">{$gan.remark}</a>
                                </td>
                                <td>{$gan.number}</td>
                            </tr>
                        {/foreach}
                    {/strip}
                </tbody>
            </table>          
        </div>
        {if $mod == 'hidden'}
        {else}
            <div id="bottom">
                <a href="/pages/business_department/give_material.php" class="button edit show">领料录入</a>
                <a href="/pages/business_department/buy_material.php" class="button edit show">采购录入</a>
                <a id="materalInfoAdd" class="button fancybox.ajax" data-fancybox-type="ajax" href="/pages/add_materialinfo.php">新增原料</a>
                <a id="materalInfoEdit" class="button edit fancybox.ajax" data-fancybox-type="ajax" href="/pages/add_materialinfo.php">修改</a>
                <a class="button del" onclick="materialInfoDelete();" href="javascript:;">删除</a>
            </div>
        {/if}
    </body>
</html>
