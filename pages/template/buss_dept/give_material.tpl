<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8" />
        <title>客户订单</title>
        {include file="head_iframe.tpl"}
    </head>
    <body class="iframe_body">
        <div id="iframe_height_set" style='width: 550px;margin:0 auto'>

            <div class="gs-label">领料车间：</div>

            <div class="gs-texts" style="width: 197px;">
              <!--  <input type="text" id="apply_input" value="" />-->
               <select id="department_select">  
                {foreach from=$departments item=dep}
                    <option value ="{$dep.department_id}" >{$dep.department_name}</option>
                {/foreach}
            </select> 
            </div>
            
            <div class='clearfix' style='margin:10px 0;'>
                <a class="button" id="materialSpeard-btn" class="button fancybox.ajax" data-fancybox-type="ajax" href="/pages/public/material_list.php" 
                   style='margin: 0;display: inline-block;padding:2px 0;width: 200px;float:left;'>点击选择原料</a>
               <!-- <a id="materalInfoAdd" data_add="1" class="button edit fancybox.ajax" data-fancybox-type="ajax" href="/pages/add_materialinfo.php" onclick="give_material();" 
                   style="display: inline-block;width: 200px;padding: 2px 0;margin:0;float:right;">点击录入新原料</a>-->
            </div>
            
            <div id='productListWrap' style='display: none'>
                <div class="gs-label">领取原料 <span id="order_sum" style="font-size: 16px;color:#900;">&yen;0</span></div>
                <table id='productListSelected' class='tbStyle' width="100%" style='margin-top: 0;margin-bottom:15px;border:1px solid #dedede;'>
                    <thead>
                        <tr style='border-left: 1px solid #dedede;'>
                            <td>原料编号</td>
                            <td style='max-width: 150px;'>原料</td>
                            <td>规格</td>
                            <td>数量</td>
                            <td></td>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>

            <div class="gs-label">备注</div>

            <span class="frm_textarea_box">
                <textarea class="js_desc frm_textarea" id="gs-form-desc"></textarea>
            </span>

            <div style="text-align: center;margin-top: 15px;">
                <a class='button edit show' href="javascript:;" onclick="giveMaterial()">确认</a>
                <a class='button del show' href="javascript:;" onclick="history.go(-1);">取消</a>
            </div>
        </div>
        <script type='text/javascript'>
            $(function() {
                resize();
                window.onresize = resize;
            });
            function resize() {
                $('#iframe_height_set').css('margin-top', ($(window).height() - $('#iframe_height_set').height()) / 2);
            }
        </script>
    </body>
</html>