<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8" />
        <title>客户订单</title>
        {include file="head_iframe.tpl"}
        <link rel="stylesheet" type="text/css" href="/scripts/js/jquery-ui-1.11.1.custom/jquery-ui.min.css" />
        <script src="/scripts/js/jquery-ui-1.11.1.custom/jquery-ui.min.js"></script>
        <script src="/scripts/js/jquery-ui-1.11.1.custom/cn.js"></script>
    </head>
    <body class="iframe_body">
        <div id="iframe_height_set" style='width: 610px;margin:0 auto'>

            <div class="clearfix">
                <div style="width:32%;float:left;margin-right:1.5%;">
                    <div class="gs-label">下单客户</div>
                    <select id="operator_type_id">  
                        {section name=sc loop=$cuslist}
                            <option value ="{$cuslist[sc].customer_id}" >{$cuslist[sc].customer_name}</option>
                        {/section}
                    </select>  
                </div>
                <div style="width:32%;float:left;margin-right:1.5%;">
                    <div class="gs-label">交付日期</div>
                    <div class="gs-text" style="margin-top: 2px;">
                        <input type="text" id="order-delay" value="" placeholder='选择交付日期' readonly="true" />
                    </div>    
                </div>
                <div style="width:32%;float:left;">
                    <div class="gs-label">代发方</div>
                    <div class="gs-text" style="margin-top: 2px;">
                        <input type="text" id="order-proxy" value="" placeholder='可选' />
                    </div>    
                </div>
            </div>

            <div style='margin:15px 0;' class='clearfix'>
                <a class="button" id="addorder-selectProduct" class="button fancybox.ajax" data-fancybox-type="ajax" href="/pages/public/productlist.php" 
                   style='margin:0;margin-right:5px;width: 135px;display: inline-block;float:left;'>选择产品</a>
                <a id="productInfoAdd" data_add="1" class="button edit fancybox.ajax" data-fancybox-type="ajax" href="/pages/add_productinfo.php" onclick="give();" 
                   style="display: inline-block;width: 125px;float:left;">添加产品</a>
            </div>

            <div id='productListWrap' style='display: none'>
                <div class="gs-label">下单商品</div>
                <table id='productListSelected' class='tbStyle' width="100%" style='margin-top: 0;margin-bottom:15px;border:1px solid #dedede;'>
                    <thead>
                        <tr style='border-left: 1px solid #dedede;'>
                            <th style='max-width: 150px;'>产品</th>
                            <th>数量</th>
                            <th>单价</th>
                            <th>总价</th>
                            <th>代发单价</th>
                            <th>代发总价</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div> 

            <div class="gs-label">备注</div>

            <span class="frm_textarea_box">
                <textarea class="js_desc frm_textarea" id="gs-form-desc" style='height: 70px;'></textarea>
            </span>

            <div style="margin-bottom: 10px;line-height: 29px;">
                总计：<span id="order_sum" class="tprice">&yen;0</span><br />
                代发总计：<span id="order_sum1" class="tprice">&yen;0</span>
            </div>       

            <div style="text-align: center;margin-top: 15px;">
                <a class='button edit' href="javascript:;" onclick="addCustomerOrder();" style='display: inline-block;'>确认下单</a>
                <a class='button del' href='/pages/business_department/customers_orders.php' style='display: inline-block;'>取消</a>
            </div>
        </div>
        <script type='text/javascript'>
            $(function () {
                isorderProxy = 0;
                resize();
                window.onresize = resize;
                initdatepicker_cn();
                $('#order-delay').datepicker({
                    //navigationAsDateFormat: true,
                    dateFormat: 'yy-mm-dd'
                });
            });
            function resize() {
                $('#iframe_height_set').css('padding-top', ($(window).height() - $('#iframe_height_set').height()) / 2);
            }
        </script>
    </body>
</html>