<div style="width: 320px;padding:20px;">
    <input type="hidden" id="cusMod" value="{$mod}" />
    <div id="modify_department_div">
        <div class="gs-label">姓名：</div>
        <div class="gs-text">
            <input type="text" id="cus_name" value="" tabindex="1"/>
        </div>
        <div class="gs-label">电话：</div>
        <div class="gs-text">
            <input type="text" id="cus_phone" value="" tabindex="2" />
        </div>
        <div class="gs-label"> QQ：</div>
        <div class="gs-text">
            <input type="text" id="cus_qq" value="" tabindex="3" />
        </div>
        <div class="gs-label">邮箱：</div>
        <div class="gs-text">
            <input type="text" id="cus_email" value="" tabindex="4" />
        </div>
        <div class="gs-label">地址：</div>
        <div class="gs-text">
            <input type="text" id="cus_address" value="" tabindex="5" />
        </div>
        <div class="gs-label">分组：</div>
        <select id="group_select1">  
            {foreach from=$levels item=dep}
                <option value ="{$dep.group_id}">{$dep.group_name}</option>
            {/foreach}
        </select>
    </div>
    <div style="margin-top: 15px;text-align: center;">
        {if $mod == 'add'}
            <a class="button edit show" href="javascript:;"  onclick="customerSave();">确认添加</a> 
        {else}
            <a class="button edit show" href="javascript:;"  onclick="customerSave();">保存修改</a>
        {/if}
    </div>
</div>