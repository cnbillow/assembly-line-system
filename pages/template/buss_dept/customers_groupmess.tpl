<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8" />
        <title>客户订单</title>
        {include file="head_iframe.tpl"}
    </head>
    <body class="iframe_body">
        <div id="iframe_height_set" class="clearfix" style='width: 80%;margin:0 auto'>
            <div style="width: 50%;float:left" id='left'>
                <div class="clearfix">
                    <div style="width: 55%;float:left">
                        <div class="gs-label">群发分组：</div>
                        <select id="swgroup" onchange="swGroup(this.value)">
                            {section name=gi loop=$levels}
                                <option value='{$levels[gi].group_id}'>{$levels[gi].group_name}</option>
                            {/section}
                        </select>
                    </div>
                    <div style="width: 40%;float:right;">
                        <div class="gs-label">公司名：</div>
                        <div class="gs-text" style="margin-top: 2px;">
                            <input type="text" id="department_name" value="汇集华科" />
                        </div>
                    </div>
                </div>
                <div style="margin-top:15px;margin-bottom:15px;overflow: auto;border:1px solid #dedede;" id="cus-wra">
                    <table id="custom_det2" width='100%' class="tbStyle" style='border-right:none;'>
                        <thead>
                            <tr style='line-height: 32px;height: 32px;'>
                                <th><input type='checkbox' id='topcheck' checked='checked' /></th>
                                <th>客户姓名</th>
                                <th style="padding-left: 0">联系电话</th>
                                    {*                                <th style="padding-left: 0"> </th>*}
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
            <div style="margin-left: 55%;" id='right'>
                <div class="gs-label">群发内容</div>
                <span class="frm_textarea_box">
                    <textarea class="js_desc frm_textarea" id="content" style='height:350px;'></textarea>
                </span>
                <div style="text-align: center;margin-top: 15px;">
                    <a class='button' href="javascript:;" onclick="customer_groupmessage();">确认群发</a>
                </div>
            </div>
        </div>
        <script type='text/javascript'>
            var check = true;
            $(function() {
                resize();
                window.onresize = resize;
                swGroup($('#swgroup').find('option').eq(0).val());

                $('#topcheck').click(function() {
                    $('#custom_det2 tbody tr').each(function(i, node) {
                        var ch = $('input', node)[0];
                        if (check) {
                            if (ch.checked) {
                                ch.checked = false;
                                $(node).click();
                            }
                        } else {
                            if (!ch.checked) {
                                ch.checked = true;
                                $(node).click();
                            }
                        }
                    });
                    check = !check;
                });
            });
            function resize() {
                $('#right').height($('#left').height());
                $('#cus-wra').css('height', ($(window).height() * 0.6) + 'px');
                $('#iframe_height_set').css('margin-top', ($(window).height() - $('#iframe_height_set').height()) / 2);
            }
            function swGroup(id) {
                $.get('/pages/ajaxJson/ajax_get_customers.php?id=' + id, function(Json) {
                    Json = Json.toJson();
                    var tr = '';
                    for (var i in Json) {
                        tr += "<tr style='cursor:pointer;line-height: 32px;height: 32px;' class='click'>";
                        tr += "<td><input type='checkbox' checked='checked'/></td>";
                        tr += '<td style="display:none;">' + Json[i].customer_id + '</td>';
                        tr += '<td>' + Json[i].customer_name + '</td>';
                        tr += '<td style="padding-left: 0" class="cusphone">' + Json[i].tel + '</td>';
                        // tr += '<td style="padding-left: 0"><a href="javascript:;" class="gDel">删除</a></td>';
                        tr += '</tr>';
                    }
                    $('#custom_det2 tbody').html(tr);
                    DataTableMuli = true;
                    dataTableLis('#custom_det2');
            {*                    $('.gDel').unbind('click').click(function() {
            $(this).parent().parent().remove();
            });*}
                    if(!check){
                        check = true;
                        $('#topcheck')[0].checked = true;
                    } 
                    resize();
                });
            }
        </script>
    </body>
</html>