<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8" />
        <title>客户订单</title>
        {include file="head_iframe.tpl"}
    </head>
    <body class="iframe_body">
        <div id="iframe_height_set">
            <div>
                <table class="dTable" width="100%">
                    <thead>
                        <tr class="custom_orders_tr">
                            <th style="display: none">订单id</th>
                            <th>订单号</th>
                            {if $usertype == 0}<th>客户</th>{/if}                         
                            <th>下单时间</th>
                            <th>交付日期</th>
                            <th>发货时间</th>
                            {if $usertype == 0}<th>金额</th>{/if}                
                            <th>状态</th>                          
                        </tr>
                    </thead>
                    <tbody>
                        {foreach from=$order item=gan}
                            <tr class="custom_orders_tr">
                                <td style="display: none"> {$gan.consumer_order_id}</td>
                                <td class="f12">{$gan.customer_order_code}</td>
                                {if $usertype == 0}<td>{$gan.customer_name}</td>{/if}       
                                <td class="f12">{$gan.create_date}</td>   
                                <td class="f12">{$gan.send_date}</td>              
                                <td class="f12">{$gan.finish_date}</td>  
                                {if $usertype == 0}<td class="prices">&yen;{$gan.sum}</td>{/if}
                                <td class="cos{$gan.order_stateint}">{$gan.order_state}</td>                  
                            </tr>
                        {/foreach}
                    </tbody>
                </table>
            </div>
        </div>
        <div id="bottom" style="text-align: center;">
            <a href="/pages/business_department/create_customer_order.php" class="button">客户下单</a>
            <a id="check_detail" class="button edit"  href="/pages/public/check_detail.php" onclick="check_detail();">查看详情</a>
            <a id="cusorder_del" class="button del" onclick="cancelCustomOrder()" href="javascript:;">取消订单</a>
        </div>
    </body>
</html>