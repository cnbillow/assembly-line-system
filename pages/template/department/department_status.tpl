<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8" />
        <title>成品信息管理</title>
        {include file="head_iframe.tpl"}
        <link rel="stylesheet" type="text/css" href="/scripts/js/jquery-ui-1.11.1.custom/jquery-ui.min.css" />
        <script src="/scripts/js/jquery-ui-1.11.1.custom/jquery-ui.min.js"></script>
        <script src="/scripts/js/jquery-ui-1.11.1.custom/cn.js"></script>
    </head>
    <body class="iframe_body" style="padding:0;">
        <div id="iframe_height_set" style="position:relative;">
            <div style="position:absolute;right:15px;top:10px;">
                <a class='button edit show' href="javascript:;" style="float:right;margin-top: 2px;" onclick="cx();">查询</a>
                <div class="gs-text"style="width: 150px;display: inline-block; float:right" >
                    <input type="text" id="order-delay2" value="" placeholder='选择查询日期' readonly="true" />
                </div>                     
                <div class="gs-text"style="width: 150px;display: inline-block; float:right;margin-right: 10px;" >
                    <input type="text" id="order-delay1" value="" placeholder='选择查询日期' readonly="true" />
                </div>   
            </div>
            {foreach from=$dept item=dep}
                <div style="border-bottom: 1px solid #dedede;padding:15px;">
                    {$dep.department_name} <a class="buttonX fancybox.ajax" data-fancybox-type="ajax" href="/pages/workshop/recordlist.php?mod=p&dept={$dep.department_id}">生产详细</a>
                    <table width="100%" style="border: 1px solid #dedede;margin-top: 15px;">
                        <thead>
                            <tr style="border-left: 1px solid #dedede;">
                                <th style="width: 25%">型号</th>
                                <th style="width: 25%">拿料</th>
                                <th style="width: 25%">产量</th>
                                <th style="width: 25%">良品率</th>
                            </tr>
                        </thead>
                        <tbody>
                            {foreach from=$dep.produce item=pd name=fo}
                                <tr style="border-left: 1px solid #dedede;">
                                    <td>
                                        {$pd.product_model}
                                    </td>
                                    <td>
                                        {$pd.getX}
                                    </td>
                                    <td>
                                        {$pd.numberX}
                                    </td>
                                    <td>
                                        {if $pd.rate}{$pd.rate}%{/if}
                                    </td>
                                </tr>
                            {/foreach}
                        </tbody>
                    </table>
                </div>
            {/foreach}
        </div>
        <script type='text/javascript'>
            $(function() {
                initdatepicker_cn();
                $('#order-delay1').datepicker({
                    //navigationAsDateFormat: true,
                    dateFormat: 'yy-mm-dd'
                });
                $('#order-delay2').datepicker({
                    //navigationAsDateFormat: true,
                    dateFormat: 'yy-mm-dd'
                });
            });
            function cx() {
                location.href = '/pages/department/department_status.php?from=' + $('#order-delay1').val() + '&to=' + $('#order-delay2').val();
            }
        </script>
    </body>
</html>