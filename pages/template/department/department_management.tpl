<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8" />
        <title>车间管理</title>
        {include file="head_iframe.tpl"}
    </head>
    <body class="iframe_body">
        <div id="iframe_height_set">
            <table class="dTable tbStyle" width="100%">
                <thead>
                    <tr>
                        <th>编号</th>
                        <th>车间</th>
                    </tr>
                </thead>
                <tbody>
                    {foreach from=$department item=gan}
                        <tr>
                            <td data-id="{$gan.department_id}">{$gan.department_id}</td>
                            <td>{$gan.department_name}</td>
                        </tr>
                    {/foreach}
                </tbody>
            </table>
        </div>
        <div id="bottom" style="text-align: center">
            <a class="button" id="add" class="button fancybox.ajax" data-fancybox-type="ajax" href="/pages/department/add_department.php?mod=add">新增车间</a>
            <a class="button edit" id="department_modify" class="button fancybox.ajax" data-fancybox-type="ajax" href="/pages/department/add_department.php?mod=alt">编辑</a>
            <a class="button del" href="javascript:;" onclick="departmentDelete();" >删除</a>
        </div>
    </body>
</html>
