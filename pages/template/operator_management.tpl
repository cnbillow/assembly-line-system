<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8" />
        <title>操作员管理</title>
        {include file="head_iframe.tpl"}
    </head>
    <body class="iframe_body">
        <div id="iframe_height_set">
            <div>               
                <table class="dTable" width="100%">
                    <thead>
                        <tr>
                            <th style="display: none">工号</th>
                            <th style="display: none">工号</th>
                            <th>姓名</th>
                            <th>身份</th>
                            <th style="display: none"> </th>
                        </tr>
                    </thead>
                    <tbody>
                        {foreach from=$operator_management item=gan}
                            <tr>
                                <td style="display: none">{$gan.user_type_id}</td>
                                <td style="display: none">{$gan.user_id}</td>
                                <td>{$gan.user_name}</td>
                                <td>{$gan.user_type_name}
                                    {if $gan.department_name}({$gan.department_name}){/if}
                                </td>
                                <td style="display: none">{$gan.department_name}</td>
                            </tr>     
                        {/foreach}
                    </tbody>
                </table>               
            </div>
        </div>
        <div id="bottom">
            <a class="button" id="add" class="button fancybox.ajax" data-fancybox-type="ajax" href="../pages/add_operator.php?mod=add">添加操作员</a>
            <a class="button edit" id="modify" class="button fancybox.ajax" data-fancybox-type="ajax" href="../pages/add_operator.php?mod=alt">修改</a>
            <a class="button del"  href="javascript:;" id="operator_delete" onclick="operatorDelete();">删除</a>
        </div>
    </body>
</html>
