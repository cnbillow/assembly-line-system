<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8" />
        <title>成品信息管理</title>
        {include file="head_iframe.tpl"}
    </head>
    <body class="iframe_body">
        <div id="iframe_height_set">
            <div>
                <table class="dTable" width="100%">
                    <thead>
                        <tr>
                            <td>商品编号</td>
                            <td>商品名称</td>
                            <td>规格</td>
                            <td>库存数量</td>
                        </tr>
                    </thead>
                    <tbody>
                        {foreach from=$product item=house}
                            <tr>
                                <td>{$house.product_id}</td>
                                <td>{$house.product_model}</td>
                                <td>{$house.gongyi}</td>
                                <td>{$house.number}</td>
                            </tr>
                        {/foreach}
                    </tbody>
                </table>
            </div>

        </div>
        <div id="bottom">
            <a class="button" onclick="productInfoAdd();" href="javascript:;">添加</a>
            <a class="button edit" onclick="productInfoAlter();" href="javascript:;">修改</a>
            <a class="button del" onclick="productInfoDelete();" href="javascript:;">删除</a>
        </div>
    </body>
</html>

