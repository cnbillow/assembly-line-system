<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="renderer" content="webkit">
<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<link href = "/scripts/css/style_iframe.css?v={$vers}" rel = "stylesheet" type = "text/css" />
<link href="/scripts/js/fancyBox/source/jquery.fancybox.css" rel = "stylesheet" type = "text/css" />
<script type = "text/javascript" src = "/scripts/js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="/scripts/js/fancyBox/source/jquery.fancybox.js"></script>
<script type="text/javascript" src="/scripts/js/DataTables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="/scripts/js/main_frame.js?v={$vers}"></script>
<script type="text/javascript" src="/scripts/js/workshop.js?v={$vers}"></script>