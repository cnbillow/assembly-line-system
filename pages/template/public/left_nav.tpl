<div class="leftNavWrap" id="left_nav">
    {* 厂长导航 *}
    {if $usertype <= 1}
        <div class="left_nav">
            <a class="item" style="background-image: url(/images/barchart.png)" href="javascript:;">统计报表</a>
            <div class="subnav" style="display: block">
                {if $usertype == 0}<a class="item" href="javascript:;" data-href="statistics/overviews">系统总览</a>{/if}
                <a class="item" href="javascript:;" data-href="statistics/stores">产品库存</a>
                <a class="item" href="javascript:;" data-href="statistics/stores_1">原料库存</a>  
                {if $usertype == 0}<a class="item" href="javascript:;" data-href="statistics/orders">订单统计</a>{/if}
                <a class="item" href="javascript:;" data-href="statistics/produce">生产统计</a>                   
            </div>
        </div>
    {/if}
    {if $usertype <= 1}
        <div class="left_nav">
            <a class="item" href="javascript:;" style="background-image: url(/images/tools.png)">生产管理</a>
            <div class="subnav">
                <a class="item" href="javascript:;" data-href="/produce/produce_orders">待处理订单</a>
                <a class="item" href="javascript:;" data-href="/produce/produce_orders_finished">已完成订单</a>
                <a class="item" href="javascript:;" data-href="/department/department_status">车间情况</a>     
                <a class="item" href="javascript:;" data-href="/department/department_management">车间管理</a>   
                {*                <a class="item" href="javascript:;" data-href="workflow_setup" >流程设置</a>*}
            </div>
        </div>
        {if $usertype <= 1}
            <div class="left_nav">
                <a class="item" href="javascript:;">销售订单</a>
                <div class="subnav">
                    <a class="item" href="javascript:;" data-href="business_department/customers_orders">未完成订单</a>
                    <a class="item" href="javascript:;" data-href="business_department/customers_orders_finished">已完成订单</a>
                    {if $usertype == 0}<a class="item" href="javascript:;" data-href="business_department/proxy_order">代发货单据</a>{/if}
                </div>
            </div>
        {/if}
        {if $usertype == 0}
            <div class="left_nav">
                <a class="item" href="javascript:;" style="background-image: url(/images/profle.png)" data-href="orders_management">客户管理</a>
                <div class="subnav">
                    <a class="item" href="javascript:;" data-href="business_department/customers_management">客户管理</a>
                    <a class="item" href="javascript:;" data-href="business_department/customers_groupmess">短信群发</a>
                </div>
            </div>
        {/if}
        <div class="left_nav">
            <a class="item" style="background-image: url(/images/stack.png)" href="javascript:;">仓库管理</a>
            <div class="subnav">
                <a class="item" href="javascript:;" data-href="department/last_storehouse" title="贴合车间的仓库">生产仓库</a>
                <a class="item" href="javascript:;" data-href="department/product_transfer_record" title="生产入库记录">入库记录</a>
                <a class="item" href="javascript:;" data-href="product_storehouse" title="最终仓库">成品仓库</a>
                <a class="item" href="javascript:;" data-href="business_department/material">原料仓库</a>
                {if $usertype == 0}<a class="item" href="javascript:;" data-href="material_trade">原料采购记录</a>{/if}
                <a class="item" href="javascript:;" data-href="business_department/bus_buy_material">采购申请审核</a>  
                <a class="item" href="javascript:;" data-href="business_department/department_material_record">车间领料记录</a>  
                {if $usertype == 0}<a class="item" href="javascript:;" data-href="product_trade">成品交易记录</a>{/if}
            </div>
        </div>

        {if $usertype == 0}
            <div class="left_nav">
                <a class="item_single" style="background-image: url(/images/profle.png)" href="javascript:;" data-href="operator_management">账号管理</a>                    
            </div>     
        {/if}

    {else if $usertype == 2 || $usertype == 0}
        {* 业务部门 *}
        <div class="left_nav">
            <a class="item" style="background-image: url(/images/stack.png)" href="javascript:;">仓库管理</a>
            <div class="subnav" style="display: block">
                <a class="item" href="javascript:;" data-href="business_department/product_storehouse">成品仓库</a>
                <a class="item" href="javascript:;" data-href="business_department/product_trade">成品交易记录</a>    
            </div>
        </div>
        <div class="left_nav">
            <a class="item_single" style="background-image: url(/images/profle.png)" href="javascript:;" data-href="business_department/customers_management">客户管理</a>
        </div>
        <div class="left_nav">
            <a class="item_single" style="background-image: url(/images/trends.png)" href="javascript:;" data-href="business_department/customers_orders">订单管理</a>
        </div>
    {else if $usertype == 3 || $usertype == 0}
        {* 采购部门 *}
        <div class="left_nav">
            <a class="item_single" id="caigou" style="background-image: url(/images/stack.png)" href="javascript:;" data-href="business_department/material">仓库管理</a>
        </div>
        <div class="left_nav">
            <a class="item_single"  style="background-image: url(/images/trends.png)" href="javascript:;" data-href="business_department/bus_buy_material">订单管理</a>
        </div>
    {/if}
</div>