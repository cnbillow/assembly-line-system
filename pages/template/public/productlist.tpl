<div style='max-height:500px;width:500px;'>
    <div style="max-height: 450px;overflow: auto;border-top: 1px solid #dedede;border-bottom: 1px solid #dedede;">
        <table id='productList' class="dTable" width="100%" style='margin-top: 0;border:1px solid #dedede;margin:-1px 0;'>
            <thead>
                <tr style='border-left: 1px solid #dedede;'>
                    <td></td>
                    <td>商品编号</td>
                    <td>商品名称</td>
                    <td>规格</td>
                    <td>库存数量</td>
                </tr>
            </thead>
            <tbody>
                {foreach from=$product item=house}
                    <tr style='border-left: 1px solid #dedede;' id='pl-{$house.product_id}'>
                        <td data-id="{$house.product_id}"><input type='checkbox' /></td>
                        <td>{$house.product_code}</td>
                        <td>{$house.product_model}</td>
                        <td>{$house.gongyi}</td>
                        <td>{$house.number}</td>
                    </tr>
                {/foreach}
            </tbody>
        </table>
    </div>
    <div style="text-align: center;margin-top: 15px;">
        <a href='javascript:;' class='button' id='productlist-addbtn'>添加</a>
    </div>
</div>