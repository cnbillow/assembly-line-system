<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8" />
        <title>客户订单</title>
        {include file="head_iframe.tpl"}
    </head>
    <body class="iframe_body">
        <div class="iframeTop">
            <a class="button" href="javascript:;" onclick="history.go(-1)">返回</a>
        </div>
        <div id="iframe_height_set" style="width:500px;line-height: 27px;margin: 0 auto;">
            <div class="gs-label" style="display: none;">订单id：<a id="state">{$state}</a></div>
            <div class="gs-label" style="display: none;">订单id：<a id="customer_order_id">{$orderDetail.consumer_order_id}</a></div>
            <div class="gs-label" >订单号：<a>{$orderDetail.customer_order_code}</a></div>
            <div class="gs-label">下单客户：{$orderDetail.customer_name}</div>
            <div class="gs-label">联系电话：{$orderDetail.tel}</div>
            <div class="gs-label">下单时间：{$orderDetail.create_date}</div>
            <div class="gs-label">订单总额：{$orderDetail.amount}</div>

            <div>
                <table width="100%" style="margin-top: 0;border: 1px solid #dedede;">
                    <thead>
                        <tr class="custom_orders_tr" style="border-left: 1px solid #dedede;">
                            <td>产品</>
                                <td>数量</td>
                                <td>单价</td>
                                <td>金额</td>                     
                        </tr>
                    </thead>
                    <tbody>
                        {foreach from=$order item=gan}
                            <tr class="custom_orders_tr" style="border-left: 1px solid #dedede;">
                                <td id="products" data-id="{$gan.product_id}">{$gan.product_model}</td>
                                <td>{$gan.number}</td>
                                <td>{$gan.unit_price}</td>              
                                <td>{$gan.total_price}</td>                 
                            </tr>
                        {/foreach}
                    </tbody>
                </table>
            </div>
            <div class="check_detail_buttongroup">
                <a class="button" id="wait_for_produce" onclick="update_state($(this).text());">待生产</a>
                <a class="button" id="wait_for_send" onclick="update_state($(this).text());">待发货</a>
                <a class="button" id="already_send"  onclick="update_state($(this).text());">已发货</a>
                <a class="button" id="finish_order"  onclick="update_state($(this).text());">完成订单</a>
            </div>
        </div>
        <script type='text/javascript'>
            $(function() {
                resize();
                window.onresize = resize;
            });
            function resize() {
                $('#iframe_height_set').css('margin-top', ($(window).height() - $('#iframe_height_set').height() - $('#return').height()) / 2);
            }
        </script>
    </body>
</html>