<div style='max-height:500px;width:500px;'>
    <div style="max-height: 450px;overflow: auto;border-top: 1px solid #dedede;border-bottom: 1px solid #dedede;">
        <table id='productList' class="dTable" width="100%" style='margin-top: 0;border:1px solid #dedede;'>
            <thead>
                <tr style='border-left: 1px solid #dedede;'>
                    <td></td>
                    <td>原料编号</td>
                    <td style="display: none;">原料id</td>
                    <td>原料名称</td>
                    <td>规格</td>
                    <td>库存数量</td>
                </tr>
            </thead>
            <tbody>
                {foreach from=$material item=house}
                    <tr style='border-left: 1px solid #dedede;' id='pl-{$house.material_id}'>
                        <td><input type='checkbox' /></td>
                        <td>{$house.material_code}</td>
                        <td style="display: none;">{$house.material_id}</td>
                        <td>{$house.material_name}</td>
                        <td>{$house.material_type}</td>
                        <td>{$house.number}</td>
                    </tr>
                {/foreach}
            </tbody>
        </table>
    </div>
    <div style="text-align: center;margin-top: 15px;">
        <a href='javascript:;' class='button edit show' id='productlist-addbtn'>添加</a>
    </div>
</div>