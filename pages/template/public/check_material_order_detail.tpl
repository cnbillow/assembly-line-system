<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8" />
        <title>客户订单</title>
        {include file="head_iframe.tpl"}
    </head>
    <body class="iframe_body">
        <div class="iframeTop">
            <a class="button" href="javascript:;" onclick="history.go(-1);">返回</a>
        </div>
        <div id="iframe_height_set" style="width:500px;line-height: 27px;margin: 0 auto;">
            <div class="gs-label" style="display: none;">订单id：<a id="customer_order_id">{$Mmess.material_demand_id}</a></div>
            <div class="gs-label" style="display: none;">状态：<a id="material_state">{$state}</a></div>
            <div class="gs-label" >订单号：<a>{$Mmess.material_demand_code}</a></div>
            <div class="gs-label">申请人：{$Mmess.apply}</div>
            <div class="gs-label">下单时间：{$Mmess.create_date}</div>
            <div>
                <table width="100%" style="margin-top: 0;border: 1px solid #dedede;">
                    <thead>
                        <tr class="custom_orders_tr" style="border-left: 1px solid #dedede;">
                            <td>原料编号</td>
                            <td>原料名称</td>
                            <td>数量</td>                
                        </tr>
                    </thead>
                    <tbody>
                        {foreach from=$Mitem item=gan}
                            <tr class="custom_orders_tr" style="border-left: 1px solid #dedede;">
                                <td style="display: none;" data-id="{$gan.material_id}"></td>
                                <td>{$gan.material_code}</td>
                                <td>{$gan.material_name}</td>
                                <td>{$gan.number}</td>                
                            </tr>
                        {/foreach}
                    </tbody>
                </table>
            </div>
            {if  $usertype == 3}
            <div class="check_detail_buttongroup">
                <a class="button finish_order"  onclick="update_material_order_state($(this).text());">完成订单</a>
            </div>
            {else if $usertype == 1}
            <div class="check_detail_buttongroup">
                <a class="button" id="wait_for_produce" onclick="update_material_order_state($(this).text());">通过审核</a>
                <a class="button" id="wait_for_send" onclick="update_material_order_state($(this).text());">未通过审核</a>
                <a class="button finish_order" c  onclick="update_material_order_state($(this).text());">完成订单</a>
            </div>
            {/if}
        </div>
        <script type='text/javascript'>
            $(function() {
                resize();
                window.onresize = resize;
            });
            function resize() {
                $('#iframe_height_set').css('margin-top', ($(window).height() - $('#iframe_height_set').height() - $('#return').height()) / 2);
            }
        </script>
    </body>
</html>