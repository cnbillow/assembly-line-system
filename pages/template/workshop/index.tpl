<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8" />
        <title>分割车间</title>
        {include file="head_iframe.tpl"}
        <style type="text/css">
            .ws-infowrap:first-child{
                border-right: 1px solid #dedede;margin-left: -1px;
            }
            .h50{
                padding: 15px;
            }
            .h50:first-child{
                padding-bottom: 0;
            }
            .h50:last-child{
                padding-top: 0;
            }
            .hHead{
                border-bottom: 1px solid #4192E1;
            }
            .hHead b{
                font-size: 14px;
                font-weight: normal;
                padding-bottom: 8px;
                display: inline-block;
            }
            #plist,#glist{
                overflow: auto;
            }
            tr{
                border-left: 1px solid #dedede !important;
                border-right: 1px solid #dedede !important;
            }
        </style>
    </head>
    <body class="iframe_body">
        <input type="hidden" id="deptid" value="{$departmentId}" />
        <div id="iframe_height_set">
            <div class="ws-infowrap">
                <div class="h50">
                    <div class="clearfix">
                        <div style="width:21%;float:left;">
                            <div class="gs-label" style="margin-bottom:2px;">取料数量</div>
                            <div class="gs-text">
                                <input type="telephone" id="gnumber" />
                            </div>
                        </div>
                        <div style="width:21%;float:left;margin-left:1%">
                            <div class="gs-label">生产编号</div>
                            <select id="pdcode1">  
                                {foreach from=$pCode item=code}
                                    <option value ="{$code.product_order_id}" >{$code.product_order_code}</option>
                                {/foreach}
                            </select> 
                        </div>
                        <div style="width:22%;float:right;">
                            <div class="gs-label">取料车间</div>
                            <select id="gdept">  
                                {foreach from=$departments item=dep}
                                    {if $dep.department_id ne $departmentId}
                                        <option value ="{$dep.department_id}" >{$dep.department_name}</option>
                                    {/if}
                                {/foreach}
                            </select> 
                        </div>
                        <div style="width:33%;float:right;margin-right: 1%">
                            <div class="gs-label">产品型号</div>
                            <select id="gmod">  
                                {foreach from=$products item=dep}
                                    <option value ="{$dep.product_id}" >{$dep.product_model}({$dep.gongyi})</option>
                                {/foreach}
                            </select> 
                        </div>
                    </div>

                    <div style="text-align:center;margin-top: 15px;">
                        <a class="button edit show" style="width:60px" href="javascript:;" onclick="submitGRecord();">提交</a>
                    </div>

                </div>
                <div class="h50">
                    <div class="hHead">
                        <b>取料记录</b>
                    </div>
                    <div id="glist">

                    </div>
                </div>
            </div>
            <div class="ws-infowrap">
                <div class="h50">
                    <div class="clearfix">
                        <div style="width:35%;float:left;">
                            <div class="gs-label" style="margin-bottom:2px;">生产数量</div>
                            <div class="gs-text">
                                <input type="telephone" id="pnumber" />
                            </div>
                        </div>
                        <div style="width:28%;float:left;margin-left:1%">
                            <div class="gs-label">生产编号</div>
                            <select id="pdcode2">  
                                {foreach from=$pCode item=code}
                                    <option value ="{$code.product_order_id}" >{$code.product_order_code}</option>
                                {/foreach}
                            </select> 
                        </div>
                        <div style="width:35%;float:right;">
                            <div class="gs-label">产品型号</div>
                            <select id="pmod">  
                                {foreach from=$products item=dep}
                                    <option value ="{$dep.product_id}" >{$dep.product_model}({$dep.gongyi})</option>
                                {/foreach}
                            </select> 
                        </div>
                    </div>
                    <div style="text-align:center;margin-top: 15px;">
                        <a class="button edit show" style="width:60px" href="javascript:;" onclick="submitPRecord();">提交</a>
                    </div>
                </div>
                <div class="h50">
                    <div class="hHead">
                        <b>生产记录</b>
                    </div>
                    <div id="plist">

                    </div>
                </div>
            </div>
        </div>
        <script type='text/javascript'>
            $(function () {
                resize();
                window.onresize = resize;
                loadGRecord();
                loadPRecord();
            });
            function resize() {
                $('#iframe_height_set').height(document.documentElement.clientHeight);
                $('.ws-infowrap').height($('#iframe_height_set').height());
                $('.h50').each(function (i, node) {
                    if (i === 2) {
                        $(node).height($('.h50').eq(0).height());
                    } else if (i === 1 || i === 3) {
                        $(node).height($(node).parent().height() - 45 - $('.h50').eq(0).height());
                    }
                });
                $('#plist').height($('#plist').parent().height() - 15);
                $('#glist').height($('#glist').parent().height() - 15);
            }
        </script>
    </body>
</html>