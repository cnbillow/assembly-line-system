<?php

include dirname(__FILE__) . '/../class/_core.php';
include dirname(__FILE__) . '/../class/Material_record.php';
$row=  Material_record::get_a_page_record(0, 0);

foreach($row as &$od){
    $od['trade_date'] = Util::timeConv($od['trade_date']);
}

$Smarty->assign('material',$row);
$Smarty->display('material_trade.tpl');