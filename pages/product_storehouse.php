<?php

include dirname(__FILE__) . '/../class/_core.php';
include dirname(__FILE__) . '/../class/Product.php';

$row = Product::get_a_page_product(0, 0);
$count = Product::getProductCount();

$Smarty->assign('count',$count);
$Smarty->assign('product',$row);
$Smarty->display('product_storehouse.tpl');