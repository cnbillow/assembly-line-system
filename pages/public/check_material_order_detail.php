<?php
include dirname(__FILE__) . '/../../class/_core.php';
include dirname(__FILE__) . '/../../class/Material_demand.php';
include dirname(__FILE__) . '/../../class/Material_demand_item.php';

$material_demand_id = $_GET['id'];
$state = $_GET['state'];
//console.log($_GET['mod']);
$Mmess = Material_demand::get_material_demand($material_demand_id);
$Mitem = Material_demand_item::getData($material_demand_id);

$uid = intval($_COOKIE['usertype']);
$Smarty->assign('usertype', $_COOKIE['usertype']);
$Smarty->assign('state', $state);
$Smarty->assign('Mmess', $Mmess[0]);
$Smarty->assign('Mitem', $Mitem);
$Smarty->display('public/check_material_order_detail.tpl');