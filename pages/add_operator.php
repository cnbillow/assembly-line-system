<?php

include dirname(__FILE__) . '/../class/_core.php';

$opers = Db::get_instance()->query("SELECT * FROM fac_user_type;");
$departments = Db::get_instance()->query("SELECT * FROM fac_department WHERE `hidden` = 'no';");
        
$Smarty->assign('mod', $_GET['mod']);
$Smarty->assign('opts', $opers);
$Smarty->assign('departments',$departments);
$Smarty->display('add_operator.tpl');
