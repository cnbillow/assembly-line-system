<?php

include dirname(__FILE__) . '/../../class/_core.php';
include dirname(__FILE__) . '/../../class/customer_order.php';

$row = Consumer_order::get_orders(0, 0, "WHERE `order_state` <> '已完成'");

foreach ($row as &$od) {
    $od['create_date'] = Util::timeConv($od['create_date']);
    if ($od['finish_date'] == NULL) {
        $od['finish_date'] = '无';
    } else {
        $od['finish_date'] = Util::timeConv($od['finish_date']);
    }    
    $od['sum'] = number_format($od['sum'], 2);
}

$Smarty->assign('order', $row);
$Smarty->assign('usertype', $_COOKIE['usertype']);
$Smarty->display('buss_dept/customers_orders.tpl');
