<?php

include dirname(__FILE__) . '/../../class/_core.php';
include dirname(__FILE__) . '/../../class/Material_demand.php';

$row = Material_demand::get_orders();

foreach ($row as &$od) {
    $od['create_date'] = Util::timeConv($od['create_date']);
    if($od['finish_date'] == NULL){
        $od['finish_date'] = '无';
    } else {
        $od['finish_date'] = Util::timeConv($od['finish_date']);
    }
}

$Smarty->assign('order', $row);
$Smarty->display('buss_dept/bus_buy_material.tpl');