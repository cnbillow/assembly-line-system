<?php
include $_SERVER['DOCUMENT_ROOT'] . '/class/_core.php';
include $_SERVER['DOCUMENT_ROOT'] . '/class/Material.php';
include dirname(__FILE__) . '/../../class/Customer.php';
$row=Material::getMaterialsList();
$cusList = Customer::getAllCustomers();
$departments = Db::get_instance()->query("SELECT * FROM fac_department;");

$Smarty->assign('cuslist', $cusList);
$Smarty->assign('material',$row);
$Smarty->assign('departments',$departments);
$Smarty->display('buss_dept/give_material.tpl'); 