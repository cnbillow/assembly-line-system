<?php

include dirname(__FILE__) . '/../class/_core.php';
include dirname(__FILE__) . '/../class/User_type.php';

$uid = intval($_COOKIE['usertype']);
$user = User::get_user($_COOKIE['userid']);

$userType = User_type::get_user_type($uid);
$userDepartment = User_type::getUserDepartment($_COOKIE['userid']);

$Smarty->assign('today', date('Y年m月d日'));
$Smarty->assign('userDepartment', $userDepartment);
$Smarty->assign('userName', $user->user_name);
$Smarty->assign('usertypeName', $userType->user_type_name);
$Smarty->assign('usertype', $_COOKIE['usertype']);
$Smarty->display('facManager.tpl');
