<?php

include $_SERVER['DOCUMENT_ROOT'] . '/class/_core.php';
include $_SERVER['DOCUMENT_ROOT'] . '/class/Stats/Stats.php';
include $_SERVER['DOCUMENT_ROOT'] . '/class/Stats/ProductStats.php';

$QueryMonth = isset($_GET['date']) ? $_GET['date'] : date("Y-m");

$orderStat = new ProductStats($QueryMonth);
$orderStat->getProduceData();