<?php

include dirname(__FILE__) . '/../../class/_core.php';
include dirname(__FILE__) . '/../../class/department.php';

$department = new department();
$view = $department->department_view_all();

foreach ($view AS &$v) {
    $v['rate'] = sprintf('%.2f', $v['rate']);
}

# var_dump($view);

$Smarty->assign('department', $view);
$Smarty->display('department/department_management.tpl');
