<?php

include dirname(__FILE__) . '/../../class/_core.php';
include dirname(__FILE__) . '/../../class/Product_order.php';

$order = Product_order::get_orders(0, 0, "WHERE `order_state` <> '已完成'");

foreach ($order as &$od) {
    $od['create_date'] = Util::timeConv($od['create_date']);
    $od['finish_date'] = Util::timeConv($od['finish_date']);
}

$Smarty->assign('finishOrderList', false);
$Smarty->assign('order', $order);
$Smarty->display('produce/produce_orders.tpl');