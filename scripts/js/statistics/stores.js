/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(function() {
    $.get('/pages/statistics/ajax/ajax_getproductstore.php', function(r) {
        r = r.toJson();
        r.b = arrayInt(r.b);
        $('#stores_1').highcharts({
            chart: {
                type: 'column',
                margin: [30, 35, 90, 70],
                style: {
                    fontFamily: '"Microsoft YaHei"',
                    fontSize: '12px'
                },
                width: $(window).width() - 10,
                height: $(window).height() * 0.54 - 47
            },
            title: {
                text: ' '
            },
            xAxis: {
                categories: r.a,
                labels: {
                    rotation: -45,
                    align: 'right',
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: '目前库存 (件)'
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                pointFormat: '目前库存: <b>{point.y:.1f}</b>',
            },
            series: [{
                    name: '库存',
                    data: r.b,
                    dataLabels: {
                        enabled: true,
                        rotation: -90,
                        color: '#FFFFFF',
                        align: 'right',
                        x: 4,
                        y: 10,
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif',
                            textShadow: '0 0 3px black'
                        }
                    }
                }]
        });

        $('#product_orders').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                style: {
                    fontFamily: '"Microsoft YaHei"', // default font
                    fontSize: '12px'
                },
                height: $(window).height() * 0.46,
                width: $(window).width() - 10
            },
            title: {
                text: ' '
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            series: [{
                    type: 'pie',
                    name: '占比',
                    data: r.c
                }]
        });
    });
});