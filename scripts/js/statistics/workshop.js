$(function() {
    $.get('/pages/statistics/ajax/ajax_getworkshoprate.php', function(r) {
        r = r.toJson();
        var colors = Highcharts.getOptions().colors;
        var categories = r.a, name = ' ';
        var data = [];
        for(var i in r.b){
            data.push({
                y : r.b[i],
                color : colors[i]
            });
        }
        $('#container').highcharts({
            chart: {
                type: 'column',
                style: {
                    fontFamily: '"Microsoft YaHei"',
                    fontSize: '12px'
                }
            },
            title: {
                text: '本月车间生产合格率'
            },
            xAxis: {
                categories: categories
            },
            yAxis: {
                title: {
                    text: '合格率'
                }
            },
            plotOptions: {
                column: {
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        color: colors[0],
                        style: {
                            fontWeight: 'bold'
                        },
                        formatter: function() {
                            return this.y + '%';
                        }
                    }
                }
            },
            tooltip: {
                formatter: function() {
                    var point = this.point,
                            s = this.x + ' : 合格率 <b>' + this.y + '% </b><br>';
                    return s;
                }
            },
            series: [{
                    name: name,
                    data: data,
                    color: 'white'
                }],
            exporting: {
                enabled: false
            }
        });
    });
});