<?php

include_once dirname(__FILE__) . '/Db.php';

class Product_record {

    private $trade_id;
    private $product_id;
    private $trade_date;
    private $trade_number;

    private function __construct() {
        $args_num = func_num_args(); //获取参数个数
        $args = func_get_args(); //获取参数列表

        switch ($args_num) {
            case 1:
                $this->__construct1($args[0]);
                break;
            case 2:
                $this->__construct2($args[1]);
                break;
            default:

                break;
        }
    }

    //根据id构造函数
    private function __construct1($id) {

        $db = Db::get_instance();
        $sql = "SELECT * FROM  fac_product_record where trade_id=" . $id;
        $rs = $db->query($sql);
        $row = $rs[0];
        $this->trade_id = $row['trade_id'];
        $this->product_id = $row['product_id'];
        $this->trade_number = $row['trade_number'];
        $this->trade_date = $row['trade_date'];
    }

    //根据数据库查询结果的构造函数
    private function __construct2($row) {
        $this->trade_id = $row['trade_id']; //
        $this->product_id = $row['product_id'];
        $this->trade_number = $row['trade_number'];
        $this->trade_date = $row['trade_date'];
    }

    //根据id得到一个产品交易记录对象
    public static function get_Customer($id) {
        $id = intval($id);
        if (Product_record::id_is_exist($id))
            return new Product_record($id);
        else
            return false;
    }

    //根据id判断产品交易记录是否存在
    public static function id_is_exist($id) {
        $id = intval($id);
        $sql = "SELECT count(*) as num from fac_product_record where trade_id=" . $id;
        $rs = Db::get_instance()->query($sql);

        $row = $rs[0];
        if ($row["num"] < 1)
            return false;
        else
            return true;
    }

    //获取产品交易记录id
    public function get_id() {
        return $this->trade_id;
    }

    //增加一条产品交易记录
    public static function add_a_record($products) {
        //if(User::is_name_exist($c_name)) return false;
        $sql = "INSERT INTO fac_product_record (product_id,trade_date,trade_number,price) values ";
        foreach ($products as $product_item) {
            $product_id = $product_item['product_id']; //成品id
            $number = $product_item ['number']; //成品数量         
            $price = $product_item['unit_price']; //价格 
            $sum = $price * $number;
            $sql.="($product_id,now(),$number,$sum),";
        }
        $sql = substr($sql, 0, strlen($sql) - 1);
        return Db::get_instance()->query($sql) > 0;
    }

    //分页获取产品交易记录列表
    public static function get_a_page_record($num_per_page, $page_index, $orderby = 'trade_date') {
        
        if($num_per_page == 0){
            $limit = '';
        } else {
            $num1 = $page_index * $num_per_page;
            $limit = "limit $num1,$num_per_page;";
        }

        $sql = "SELECT * from fac_product_record t1 LEFT JOIN fac_product_mess t2 on t1.product_id=t2.product_id  order by $orderby $limit";

        $rs = Db::get_instance()->query($sql);

        return $rs;
    }

    //删除一条产品交易记录
    public static function del_a_record($id) {
        $id = intval($id);
        $sql = 'DELETE from fac_product_record where trade_id=' . $id;
        $rs = Db::get_instance()->query($sql);
        if ($rs)
            return true;
        else
            return false;
    }

}
