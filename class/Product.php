<?php

include_once dirname(__FILE__) . '/Db.php';

class Product {

    private $product_id;
    public $product_model;
    public $product_material;
    public $gongyi;
    public $number;
    public $product_code;

    private function __construct() {
        $args_num = func_num_args(); //获取参数个数
        $args = func_get_args(); //获取参数列表

        switch ($args_num) {
            case 1:
                $this->__construct1($args[0]);
                break;
            case 2:
                $this->__construct2($args[1]);
                break;
            default:

                break;
        }
    }

    //根据id构造函数
    private function __construct1($id) {

        $db = Db::get_instance();
        $sql = "SELECT * FROM  fac_product_mess where product_id=" . $id;
        $rs = $db->query($sql);
        $row = $rs[0];
        $this->product_id = $row['product_id'];
        $this->product_model = $row['product_model'];
        $this->product_material = $row['product_material'];
        $this->gongyi = $row['gongyi'];
        $this->number = $row['number'];
        $this->product_code = $row['product_code'];
    }

    //根据数据库查询结果的构造函数
    private function __construct2($row) {
        $this->product_id = $row['product_id'];
        $this->product_model = $row['product_model'];
        $this->product_material = $row['product_material'];
        $this->gongyi = $row['gongyi'];
        $this->number = $row['number'];
        $this->product_code = $row['product_code'];
    }

    //根据id得到一个成品对象
    public static function get_Product($id) {
        $id = intval($id);
        if (Product::id_is_exist($id))
            return new Product($id);
        else
            return false;
    }

    //根据id判断成品是否存在
    public static function id_is_exist($id) {
        $id = intval($id);
        $sql = "SELECT count(*) as num from fac_product_mess where product_id=" . $id;
        $rs = Db::get_instance()->query($sql);

        $row = $rs[0];
        if ($row["num"] < 1)
            return false;
        else
            return true;
    }

    //获取成品id
    public function get_id() {
        return $this->product_id;
    }

    /**
     * 增加一种成品 
     */
    public static function add_a_product($c_product_code,$c_mode, $c_material, $c_gongyi, $c_number) {
        $sql = "INSERT INTO fac_product_mess (product_code,product_model,product_material,gongyi,number)";
        $sql.=" value ('$c_product_code','$c_mode','$c_material','$c_gongyi',$c_number)";
        return Db::get_instance()->query($sql);
        
    }

    //判断数据库中是否已存在产品名
    public static function is_name_exist($model) {
        $sql = 'SELECT count(*) as num from fac_product_mess WHERE hidden="no" AND product_model=\'' . $model . '\'';
        $rs = Db::get_instance()->query($sql);
        $row = $rs[0];
        $num = $row['num'];
        return $num > 0;
    }

    //分页获取产品列表
    public static function get_a_page_product($num_per_page, $page_index) {
        //page_index代表页的下标  从0开始
        $num1 = $page_index * $num_per_page;
        if ($num_per_page == 0) {
            $limit = '';
        } else {
            $limit = "limit $num1,$num_per_page";
        }

        $sql = "SELECT * from fac_product_mess WHERE `hidden` = 'no' $limit;";
        return Db::get_instance()->query($sql);
    }

    //删除一个产品
    public static function delete_a_product($id) {
        $id = intval($id);
        $sql = 'DELETE from fac_product_mess where product_id=' . $id;
        $rs = Db::get_instance()->query($sql);
        if ($rs) {
            return true;
        } else {
            $sql = "UPDATE fac_product_mess SET hidden='yes' WHERE product_id=$id";
            return Db::get_instance()->query($sql);
        }
    }

    //修改内容后更新一个产品
    /**
     * 	
     */
    public function update() {
        $sql = 'update fac_product_mess set product_model=\'' . $this->product_model . '\',';
        $sql.='product_material=\'' . $this->product_material . '\',';
        $sql.='gongyi=\'' . $this->gongyi . '\',';
        $sql.='product_code=\'' . $this->product_code . '\',';
        $sql.='number=' . $this->number;
        
        $sql.=' where product_id=' . $this->product_id;
        $rs = Db::get_instance()->query($sql) . "<br>";
        if ($rs)
            return true;
        else
            return false;
    }

    /**
     * 获取产品总数
     * @return type
     */
    public static function getProductCount() {
        $sql = "SELECT SUM(number) AS count FROM `fac_product_mess` WHERE `hidden` = 'no';";
        $res = Db::get_instance()->query($sql);
        return $res[0]['count'];
    }

}
