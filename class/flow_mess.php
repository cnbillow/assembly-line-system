<?php

include_once dirname(__FILE__) . '/Db.php';

class flow_mess {

    public $id;
    public $flow_id;
    public $department_id;
    public $sort;

    public function __construct() {
        $this->db = Db::get_instance();
    }

    public function is_name_exit($flow_id, $department_id) {

        $sql = "SELECT department_id= $department_id FROM fac_flow_mess WHERE   flow_id=$flow_id";
        if (Db::get_instance()->query($sql) > 0) {
            return 1;
        } else {
            return 0;
        }
    }

    //增加一个流水线
    function insert($flow_id, $department_id, $sort) {
        if (is_name_exit($flow_id, $department_id)) {
            $insert = "insert into fac_flow_mess(flow_id,department_id,sort) values($flow_id,$department_id,$sort);";
            return $this->db->query($insert);
        } else {

            return 0;
        }
    }

    //修改一个流水线
    function update($id, $flow_id, $department_id, $sort) {
        $update = "UPDATE fac_flow_mess SET flow_id=$flow_id,department_id=$department_id,sort=$sort WHERE id=$id;";
        return $this->db->query($update);
    }

    //删除一条流水线
    function delete($id) {
        $delete = "delete from fac_flow_mess where id=$id";
        return $this->db->query($delete);
    }

    //查看流水线
    function view($id) {
        if ($id) {
            $sql = "SELECT * FROM fac_flow_mess t1 LEFT JOIN fac_flow t2 ON t1.flow_id=t2.flow_id LEFT JOIN fac_department t3 ON t1.department_id=t3.department_id WHERE t1.flow_id=$id;";
        } else {
            $sql = "SELECT * FROM fac_flow_mess t1 LEFT JOIN fac_flow t2 ON t1.flow_id=t2.flow_id LEFT JOIN fac_department t3 ON t1.department_id=t3.department_id;";
        }
        return $this->db->query($sql);
    }

}
