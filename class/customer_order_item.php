<?php
include_once dirname(__FILE__) . '/Db.php';

class Consumer_order_item {

    private $consumer_order_item_id;
    public $product_id;
    public $number;
    public $consumer_order_id;
	public $unit_price;

    public function __construct() {
        $args_num = func_num_args(); //获取参数个数
        $args = func_get_args(); //获取参数列表

        switch ($args_num) {
            case 1:
                $this->__construct1($args[0]);
                break;
            case 2:
                $this->__construct2($args[1]);
                break;
            default:

                break;
        }
    }

    private function __construct1($id) {

        $db = Db::get_instance();
        $sql = "SELECT * FROM  fac_consumer_order_item where consumer_order_item_id=" . $id;
        $rs = $db->query($sql);
        $row = $rs[0];
        $this->consumer_order_item_id = $row['consumer_order_item_id'];
        $this->product_id = $row['product_id'];
        $this->number = $row['number'];
        $this->consumer_order_id = $row['consumer_order_id']; 
		$this->unit_price = $row['unit_price'];
    }

    //根据数据库查询结果的构造函数
    private function __construct2($row) {
        $this->consumer_order_item_id = $row['consumer_order_item_id'];
        $this->product_id = $row['product_id'];
        $this->number = $row['number'];
        $this->consumer_order_id = $row['consumer_order_id'];
		$this->unit_price= $row['unit_price'];
    }

    //根据id得到一个订单条目对象
    public static function get_Consumer_order_item($id) {
        $id = intval($id);
        if (Consumer_order_item::id_is_exist($id))
            return new Consumer_order_item($id);
        else
            return false;
    }

    //根据id判断订单条目是否存在
    public static function id_is_exist($id) {
        $id = intval($id);
        $sql = "SELECT count(*) as num from fac_consumer_order_item where consumer_order_item_id=" . $id;
        $rs = Db::get_instance()->query($sql);

        $row = $rs[0];
        return $row["num"] > 0;
          
    }

    //获取订单条目id
    public function get_id() {
        return $this->consumer_order_item_id;
    }

    //增加一个订单条目
    public static function add_a_consumer_order_item($c_pid, $c_number,$c_unit_price, $c_consumer_order_id) {
        $sql = "INSERT INTO fac_consumer_order_item (product_id,number,unit_price,consumer_order_id)";
        $sql.="value ($c_pid,$c_number,$c_unit_price,$c_consumer_order_id)";
        //echo $sql;
        $rs = Db::get_instance()->query($sql);
        if ($rs)
            return true;
        else
            return false;
    }

    //删除一个订单条目
    public static function delete_a_consumer_order_item($id) {
        $id = intval($id);
        $sql = 'DELETE from fac_consumer_order_item where consumer_order_item_id =' . $id;
        $rs = Db::get_instance()->query($sql);
        if ($rs)
            return true;
        else
            return false;
    }

    //修改内容后更新一个
    public function update() {
        $sql = 'update fac_consumer_order_item set product_id =' . $this->product_id . ',';
        $sql.='consumer_order_id=' . $this->consumer_order_id . ',';
        $sql.='number=' . $this->number.',';
		$sql.='unit_price='.$this->unit_price;
        $sql.=' where consumer_order_item_id=' . $this->consumer_order_item_id;
        $rs = Db::get_instance()->query($sql);
        if ($rs)
            return true;
        else
            return false;
    }

    //分页获取某个订单的条目列表
    public static function get_a_page_item($id, $orderby = 'number') {
        $sql = "SELECT  * from fac_consumer_order_item where consumer_order_id=" . $id . " order by $orderby";
        $rs = Db::get_instance()->query($sql);   
        return $rs;
    }

}