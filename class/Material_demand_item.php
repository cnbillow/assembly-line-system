<?php

include_once dirname(__FILE__) . '/Db.php';

class Material_demand_item {

    private $material_demand_item_id;
    public $material_demand_id;
    public $material_id;
    public $number;

    private function __construct($id) {

        $db = Db::get_instance();
        $sql = "SELECT * FROM  fac_material_demand_item where material_demand_item_id=" . $id;
        $rs = $db->query($sql);
        $row = $rs[0];
        $this->material_demand_item_id = $row['material_demand_item_id'];
        $this->material_demand_id = $row['material_demand_id'];
        $this->number = $row['number'];
        $this->material_id = $row['material_id'];
    }

    //根据id得到一个材料需求条目对象
    public static function get_material_demand_item($id) {
        $id = intval($id);
        if (Material_demand_item::id_is_exist($id))
            return new Material_demand_item($id);
        else
            return false;
    }

    //根据id判断材料需求条目是否存在
    public static function id_is_exist($id) {
        $id = intval($id);
        $sql = "SELECT count(*) as num from fac_material_demand_item where material_demand_item_id=" . $id;
        $rs = Db::get_instance()->query($sql);

        $row = $rs[0];
        return $row["num"] > 0;
    }

    //获取订单条目id
    public function get_id() {
        return $this->material_demand_item;
    }

    //增加一个材料需求条目
    public static function add_a_material_demand_item($c_material_id, $material_demand_id, $c_number) {
        $sql = "INSERT INTO fac_material_demand_item (material_id,material_demand_id,number)";
        $sql.="values ";
        $rs = Db::get_instance()->query($sql);
        if ($rs)
            return true;
        else
            return false;
    }

    //删除一个材料需求条目
    public static function delete_a_material_demand_item($id) {
        $id = intval($id);
        $sql = 'DELETE from fac_material_demand_item where material_demand_item_id =' . $id;
        $rs = Db::get_instance()->query($sql);
        if ($rs)
            return true;
        else
            return false;
    }

    //修改内容后更新一个材料需求条目
    public function update() {
        $sql = 'update fac_material_demand_item set material_id =' . $this->material_id . ',';
        $sql.='material_demand_id=' . $this->material_demand_id . ',';
        $sql.='number=' . $this->number;
        $sql.=' where material_demand_item_id=' . $this->material_demand_item_id;
        $rs = Db::get_instance()->query($sql);
        if ($rs)
            return true;
        else
            return false;
    }

    //分页获取某个订单的条目列表
    public static function get_a_page_item($id, $orderby = 'number') {
        return Db::get_instance()->query("SELECT * from fac_material_demand_item where material_demand_id=$id order by $orderby");
    }

    public static function getData($id) {
        return Db::get_instance()->query("SELECT t2.number,t2.unit_price,(t2.number*t2.unit_price) AS totle_price,t3.material_code,t3.material_name ,t3.material_id
From fac_material_demand t1
LEFT JOIN fac_material_demand_item t2 ON t2.material_demand_id = t1.material_demand_id
LEFT JOIN fac_material_mess t3 ON t3.material_id = t2.material_id
WHERE t1.material_demand_id = $id");
    }

}
