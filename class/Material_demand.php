<?php

include_once dirname(__FILE__) . '/Material_record.php';

class Material_demand {

    public $material_demand_id;
    public $create_date;
    public $finish_date;
    public $remark;
    public $order_state;

    public function __construct($id) {
        $db = Db::get_instance();
        $sql = "SELECT * FROM  fac_material_demand where material_demand_id=" . $id;
        $rs = $db->query($sql);
        $row = $rs[0];
        $this->material_demand_id = $row['material_demand_id'];
        $this->customer_id = $row['customer_id'];
        $this->create_date = $row['create_date'];
        $this->order_state = $row['order_state'];
        $this->finish_date = $row['finish_date'];
        $this->remark = $row['remark'];
    }

    //根据id得到一个订单对象详情
    public static function get_material_demand($id) {
        return Db::get_instance()->query("SELECT * FROM  fac_material_demand where material_demand_id=" . $id);
    }

    //根据id获得一个订单对象
    public static function get_material_demand_object($id) {
        $id = intval($id);
        if (Material_demand::id_is_exist($id)) {
            return new Material_demand($id);
        } else {
            return false;
        }
    }

    //根据id判断订单是否存在
    public static function id_is_exist($id) {
        $id = intval($id);
        $sql = "SELECT count(*) as num from fac_material_demand where material_demand_id=" . $id;
        $rs = Db::get_instance()->query($sql);
        $row = $rs[0];
        return $row["num"] > 0;
    }

    //获取订单id
    public function get_id() {
        return $this->material_demand_id;
    }

    //增加一个订单
    public static function add_a_order($department_id, $apply, $c_remark, $materials,$demand_date) {
        $order_state = '待审核';
        $sql = "INSERT INTO fac_material_demand (department_id,apply,material_demand_code,create_date,order_state,remark,demand_date)";
        $sql.=" value ($department_id,'$apply',CONCAT('B',CEIL(NOW() + 0)),now(),'$order_state','$c_remark','$demand_date')";
        $demand_id = Db::get_instance()->query($sql);
        return Material_demand::save_demand_order_item($demand_id, $materials);
    }

    //原料库领取操作
     public static function material_update($materials) {
         $sql="";
       foreach ($materials as $materials_item) {
            $material_id = $materials_item['material_id'];
            $number = $materials_item['number'];
            $sql.= "UPDATE fac_material_mess SET number=number-$number where material_id = $material_id;";
       }
      // echo $sql;
        return Db::get_instance()->query($sql);
    }
    
    //原料库采购操作
     public static function buy_material_update($materials) {
         $sql="";
       foreach ($materials as $materials_item) {
            $material_id = $materials_item['material_id'];
            $number = $materials_item['number'];
            $sql.= "UPDATE fac_material_mess SET number=number+$number where material_id = $material_id;";
       }
      // echo $sql;
        return Db::get_instance()->query($sql);
    }
    /**
     * 插入领取记录
     * @param type $department_id
     * @param type $materials
     */
     public static function give_material_record($department_id, $materials) {
          $sql="";
          foreach ($materials as $materials_item) {
          $material_id = $materials_item['material_id'];
          $number = $materials_item['number'];
          $sql.= "INSERT INTO fac_give_material_record(department_id,material_id,number,give_date) VALUES($department_id,$material_id,$number,now());";
          }
          return Db::get_instance()->query($sql);
     }

    //保存订单条目
    public static function save_demand_order_item($demand_id, $materials) {
        $sql = "DELETE fac_material_demand_item where material_demand_id=" . $demand_id;
        Db::get_instance()->query($sql);
        $sql = "INSERT INTO fac_material_demand_item (material_id,number,material_demand_id,unit_price) values ";
        foreach ($materials as $materials_item) {
            $material_id = $materials_item['material_id'];
            $number = $materials_item['number'];
            $unit_price = $materials_item['unit_price'] ? $materials_item['unit_price'] : 0;
            $sql.="($material_id,$number,$demand_id,$unit_price),";
        }
        $sql = substr($sql, 0, strlen($sql) - 1);
        return Db::get_instance()->query($sql) > 0;
    }

    //获取订单列表
    public static function get_orders() {
        $sql = 'SELECT * from fac_material_demand t1 LEFT JOIN fac_department t2 ON t1.department_id=t2.department_id';
        $rs = Db::get_instance()->query($sql);
        return $rs;
    }
    
        //根据车间id获取订单列表
    public static function get_orders_by_departmentid($department_id) {
        $sql = "SELECT * from fac_material_demand where department_id=$department_id";
        $rs = Db::get_instance()->query($sql);
        return $rs;
    }

    //修改后保存订单
    public function update() {
        $sql = 'UPDATE fac_material_demand set ';
        $sql.='create_date=\'' . $this->create_date . '\',';
        $sql.='order_state=\'' . $this->order_state . '\',';
        $sql.='remark=\'' . $this->remark . '\'';
        $sql.= ' where material_demand_id=' . $this->material_demand_id;
        //echo $sql;
        $rs = Db::get_instance()->query($sql);
        if (!$rs)
            return false;
        else
            return true;
    }

    public function state_update() {
        $sql = 'UPDATE fac_material_demand set ';
        $sql.='order_state=\'' . $this->order_state . '\'';
        $sql.= ' where material_demand_id=' . $this->material_demand_id;
        if (Db::get_instance()->query($sql) > 0) {
            return true;
        } else {
            return false;
        }
    }

    //删除一个订单
    public static function delete_a_material_demand($id) {
        $table = '`fac_material_demand`';
        $db = Db::get_instance();
        $status = $db->query("SELECT `order_state` FROM $table WHERE `material_demand_id` = $id;");
        if ($status[0]['order_state'] != '已采购') {
            $res = Db::get_instance()->query("DELETE FROM `fac_material_demand_item` WHERE `material_demand_id` = $id;");
            $res1 = Db::get_instance()->query("DELETE FROM $table WHERE `material_demand_id` = $id;");
            return $res && $res1;
        } else {
            return -1;
        }
    }

    //完成该订单 $materials为 array(array('material_id'=>5,'price'=>6,'trade_number'=>6),)
    public function finish($materials) {
        //在仓库中增加所有条目
        foreach ($materials as $row) {
            $number = $row['trade_number'];
            $sql = "UPDATE fac_material_mess set number=number+$number where material_id=" . $row['material_id'];
//            Db::get_instance()->query($sql);
            //在交易记录表中记录
            Material_record::add_a_record($row['material_id'], $row['price'], $row['trade_number']);
        }
        //设置订单状态为完成
        $sql = 'UPDATE fac_material_demand set finish_date=now() ,order_state=\'已采购\' where material_demand_id=' . $this->material_demand_id;
        Db::get_instance()->query($sql);
        return true;
    }
/*
 * 获取所有领取记录
 */
    public static function get_all_material_record() {
         $sql = 'SELECT t1.*,t2.material_name,t2.material_type,t3.department_name FROM fac_give_material_record t1 LEFT JOIN fac_material_mess t2 ON 
                t1.material_id=t2.material_id LEFT JOIN fac_department t3 ON t1.department_id=t3.department_id ';   
         return  Db::get_instance()->query($sql);
    }
    /*
     * 添加采购记录
     */
    public static function save_buy_material_record($operator,$materials) {
          $sql="";
          foreach ($materials as $materials_item) {
          $material_id = $materials_item['material_id'];
          $number = $materials_item['number'];
          $unit_price = $materials_item['unit_price'];
          $sql.= "INSERT INTO fac_material_record(material_id,trade_number,price,operator,trade_date) VALUES($material_id,$number,$unit_price,$operator ,now());";
          }
          return Db::get_instance()->query($sql);
     }
}