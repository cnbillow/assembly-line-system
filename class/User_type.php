<?php

include_once dirname(__FILE__) . '/Db.php';

class User_type {

    private $user_type_id;
    public $user_type_name;

    private function __construct() {
        $args_num = func_num_args();
        $args = func_get_args();

        switch ($args_num) {
            case 1:
                $this->__construct1($args[0]);
                break;
            case 2:
                $this->__construct2($args[1]);
                break;
            default:

                break;
        }
    }

    //根据id构造函数
    private function __construct1($id) {

        $db = Db::get_instance();
        $sql = "SELECT * FROM  fac_user_type  where  user_type_id=" . $id;
        $rs = $db->query($sql);
        $row = $rs[0];
        $this->user_type_id = $row['user_type_id'];
        $this->user_type_name = $row['user_type_name'];
    }

    //根据数据库查询结果的构造函数
    private function __construct2($row) {
        $this->user_type_name = $row['user_type_name'];
        $this->user_type_id = $row['user_type_id'];
    }

    //根据id得到一个用户类型对象
    public static function get_user_type($id) {
        $id = intval($id);

        if (User_type::id_is_exist($id))
            return new User_type($id);
        else
            return false;
    }

    //根据id判断用户类型是否存在
    public static function id_is_exist($id) {
        $id = intval($id);
        $sql = "SELECT count(*) as num from fac_user_type where user_type_id=" . $id;
        $rs = Db::get_instance()->query($sql);

        $row = $rs[0];
        if ($row["num"] < 1)
            return false;
        else
            return true;
    }

    //获取用户类型id
    public function get_id() {
        return $this->user_type_id;
    }

    //增加一个用户类型
    public static function add_a_user_type($c_name) {
        if (User_type::is_name_exist($c_name))
            return false;

        $sql = "INSERT INTO fac_user_type (user_type_name)";
        $sql.="value ('$c_name')";
        $rs = Db::get_instance()->query($sql);
        if ($rs)
            return true;
        else
            return false;
    }

    //判断数据库中是否已存在用户类型名
    public static function is_name_exist($name) {
        $sql = 'SELECT count(*) as num from fac_user_type where user_type_name=\'' . $name . '\'';

        $rs = Db::get_instance()->query($sql);

        $row = $rs[0];
        $num = $row['num'];
        if ($num > 0)
            return true;
        else
            return false;
    }

    //获取用户列表
    public static function get_list() {

        $sql = "SELECT * from fac_user_type ";
        $rs = Db::get_instance()->query($sql);
        $result = array();
        foreach ($rs as $row) {
            array_push($result, new User_type('随便填个参数', $row));
        }
        return $result;
    }

    //删除一个用户类型
    public static function unreg($id) {
        $id = intval($id);
        $sql = 'DELETE from fac_user_type where user_type_id=' . $id;
        $rs = Db::get_instance()->query($sql);
        if ($rs)
            return true;
        else
            return false;
    }

    //修改内容后更新一个用户
    public function update() {
        $sql = 'update fac_user_type set user_type_name=\'' . $this->user_type_name . '\' ';
        $sql.=' where user_type_id=' . $this->user_type_id;
        //echo $sql;
        $rs = Db::get_instance()->query($sql);
        if ($rs)
            return true;
        else
            return false;
    }

    public static function add_user_department($user_id, $department_id) {
        $sql = "INSERT INTO fac_user_department(user_id,department_id) VALUES($user_id,$department_id)";
        Db::get_instance()->query($sql);
        return true;
    }

    /**
     * 获取用户所在车间名
     * @param type $uids
     */
    public static function getUserDepartment($uid) {
        $ret = Db::get_instance()->query("SELECT `department_name` FROM `fac_user_department` fd LEFT JOIN `fac_department` fdn ON fdn.department_id = fd.department_id WHERE fd.user_id = $uid;");
        return $ret[0]['department_name'];
    }

}
