<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ProductStats
 *
 * @author Administrator
 */
class MaterialStats extends Stats {

    private $Db = false;
    private $QueryMonth = false;

    public function __construct($QueryMonth) {
        $this->Db = Db::get_instance();
        $this->QueryMonth = $QueryMonth;
    }

    public function getMaterialData() {

        $ret = $this->Db->query("select `material_name`,`number`,(`number` / (SELECT SUM(`number`) FROM `fac_material_mess` WHERE `hidden` = 'no'))*100 AS percent from `fac_material_mess` WHERE `hidden` = 'no' GROUP BY `material_id` ORDER BY number DESC LIMIT 15");
        $r1 = array();
        $r2 = array();
        $r3 = array();
        foreach ($ret as $r) {
            $r1[] = $r['material_name'];
            $r2[] = $r['number'];
            $r3[] = array($r['material_name'], floatval($r['percent']));
        }
        $this->echoJson(array(
            'a' => $r1,
            'b' => $r2,
            'c' => $r3
        ));
    }

}
