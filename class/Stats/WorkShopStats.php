<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ProductStats
 *
 * @author Administrator
 */
class WorkShopStats extends Stats {

    private $Db = false;
    private $QueryMonth = false;

    public function __construct($QueryMonth) {
        $this->Db = Db::get_instance();
        $this->QueryMonth = $QueryMonth;
    }

    public function getYesRateData() {
        $ret = $this->Db->query("select fd.department_name,COALESCE((SUM(product_yes) / SUM(product_sum)) * 100,0) AS rate from fac_order_department_mess om
LEFT JOIN fac_product_order fp on fp.product_order_id = om.product_order_id AND taskstate = '已完成'
LEFT JOIN fac_department fd ON fd.department_id = om.department_id
WHERE DATE_FORMAT(create_date,'%Y-%m') = '$this->QueryMonth'
GROUP BY fd.department_id ORDER BY rate DESC");
        $r1 = array();
        $r2 = array();
        foreach ($ret as $r) {
            $r1[] = $r['department_name'];
            $r2[] = round((float) $r['rate'], 2);
        }
        $this->echoJson(array(
            'a' => $r1,
            'b' => $r2
        ));
    }

}
