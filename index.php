<!DOCTYPE html>
<html>
    <head>
        <title>管理系统登录</title>
        <meta charset="utf-8" />
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta name="format-detection" content="telephone=no">
        <link href="/images/www.ico.la_dd375a50fc947087c09b3b2137f70a6b_32X32.ico" rel="Shortcut Icon" />
        <link href="../scripts/css/style.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="scripts/js/jquery-1.9.1.min.js"></script>
        <script type="text/javascript" src="scripts/js/main.js"></script>
    </head>
    <body class="loginBody">
        <div id="login" class="clearfix">
            <form class="login-form" id="login-frame" action="/pages/login_validate.php" method="POST">
                <img src="images/profle_1.png" style="border-radius: 50px;display: block;margin:0 auto;box-shadow: 1px 1px 2px rgba(0,0,0,0.3);"/>
                <p style='margin:0;padding:0;color:#888;margin-top: 14px;'>流水线管理系统登录</p>
                <div class='login-item' style='margin-top: 0;padding-top: 14px;'>           
                    <div class="gs-text">
                        <input type="text" tabindex="1" name="username" id="pd-form-username" placeholder="用户名"/>
                    </div>
                </div>
                <div class='login-item'>     
                    <div class="gs-text">
                        <input type="password" tabindex="2" name="password" id="pd-form-password" placeholder="密码" />
                    </div>
                </div>
                <div class='login-item'>  
                    <a class="login-gbtn" href="javascript:;">登 录</a>
                </div>
            </form> 
            <div id="login-img">
                <img style="display: block;" src="/images/background/<?php echo (int) rand(1, 3); ?>.jpg" height="433px" width="100%" />
            </div>
        </div>
        <script type="text/javascript">
            $(function() {
                $('#pd-form-username').focus();
            });
        </script>
    </body>
</html>